from django.test import TestCase, Client
from django.apps import apps
from home.apps import HomeConfig

class Tests(TestCase):
    def test_list_url_is_resolved_pageselamatdatang(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved_jquery(self):
        response = Client().get('/jquery/')
        self.assertEquals(response.status_code, 200)
    
    def test_apps(self):
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')
